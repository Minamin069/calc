package com.example.weather_report;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, GetData.AsyncResponse {

    private static final String TAG = "MainActivity";

    private Button searchButton;
    private EditText searchField;
    private TextView city;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchField = findViewById(R.id.searchField);
        city = findViewById(R.id.city);

        searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(this);

    }

    private URL buildURL(String city){
        String BASE_URL = "https://api.openweathermap.org/data/2.5/weather";
        String PARAMETER = "q";
        String PARAM_APPID = "appid";
        String appid_value = "f11f5e2e462b25dc717cc70e1cd3d3b3";

        Uri buildUri = Uri.parse(BASE_URL).buildUpon().appendQueryParameter(PARAMETER, city).appendQueryParameter(PARAM_APPID, appid_value).build();
        URL url = null;

        try {
            url = new URL(buildUri.toString());
        } catch (MalformedURLException e){
            e.printStackTrace();
        }

        Log.d(TAG, "buildURL: "+url);
        return url;
    }

    @Override
    public void onClick(View view) {
//            URL url = new URL("https://api.openweathermap.org/data/2.5/weather?lat=57.7679158&lon=40.9269141&appid=f11f5e2e462b25dc717cc70e1cd3d3b3");
        URL url = buildURL(searchField.getText().toString());
        city.setText(searchField.getText().toString());
        new GetData(this).execute(url);
    }

    @Override
    public void proccessFinish(String output) {
        Log.d(TAG, "proccessFinish: "+output);
        try {

            JSONObject resultJSON = new JSONObject(output);
            JSONObject weather = resultJSON.getJSONObject("main");
            JSONObject sys = resultJSON.getJSONObject("sys");

            TextView temp = findViewById(R.id.tempValue);
            String temp_K = weather.getString("temp");
            float temp_C = Float.parseFloat(temp_K);
            temp_C -= 273.15;
            String temp_C_string = Float.toString(temp_C);
            temp.setText(temp_C_string);

            TextView pressure = findViewById(R.id.presValue);
            String pres_b = weather.getString("pressure");
            float pres_m = Float.parseFloat(pres_b);
            pres_m *= 0.750064;
            pressure.setText(Float.toString(pres_m));

            TextView sunrise = findViewById(R.id.sunrise);
            String timeSunrise = sys.getString("sunrise");
            Locale myLocale = new Locale("ru", "RU");
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", myLocale);

            String dateString = formatter.format(new Date(Long.parseLong(timeSunrise)*1000+(60*60*1000)*3));
            sunrise.setText(dateString);

            TextView sunset = findViewById(R.id.sunset);
            String timeSunset = sys.getString("sunset");

            String dateString1 = formatter.format(new Date(Long.parseLong(timeSunset)*1000+(60*60*1000)*3));
            sunset.setText(dateString1);

        } catch (JSONException e){
            e.printStackTrace();
        }
    }
}